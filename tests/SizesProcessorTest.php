<?php namespace Solarise\RapidConfig;

class SizesProcessorTest extends \WP_UnitTestCase
{
    public function testImageSizesExist()
    {
        $posts = new SizesProcessor;
        $posts->setYAML('tests/yaml/sizes_test_1.yaml');
        $posts->init();

        $wp_sizes = \get_intermediate_image_sizes();

        $this->assertTrue(in_array('size1', $wp_sizes), 'Checking that size 1 exists');

        $this->assertTrue(in_array('size2', $wp_sizes), 'And size 2');

        $this->assertEquals(sizeof($wp_sizes), 6, 'Should have 6 items in the WP sizes array');

    }
}