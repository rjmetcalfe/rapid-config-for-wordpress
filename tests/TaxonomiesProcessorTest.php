<?php namespace Solarise\RapidConfig;

class TaxonomiesProcessorTest extends \WP_UnitTestCase
{
    public function testCustomTaxonomies()
    {
        $posts = new TaxonomiesProcessor;
        $posts->setYAML('tests/yaml/taxonomies_test_1.yaml');
        $posts->init();

        $this->assertTrue(\taxonomy_exists('document-categories'));

    }
    
}