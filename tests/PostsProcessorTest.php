<?php namespace Solarise\RapidConfig;

class PostsProcessorTest extends \WP_UnitTestCase
{
    public function testCustomPostCreation()
    {
        $posts = new PostsProcessor;
        $posts->setYAML('tests/yaml/posts_test_1.yaml');
        $posts->init();

        $this->assertTrue(\post_type_exists('document'));

        $this->assertFalse(\post_type_exists('blahblahblah'));

    }
    
}