<?php namespace Solarise\RapidConfig;

class FieldsProcessor extends Processor implements IProcessor {

	public $name = 'fields';

	public function __construct()
	{
		// check if ACF is loaded
		// if not, include it
		if( ! class_exists('acf') ) {
			// Load the ACF Core
			include_once('/advanced-custom-fields/acf.php');
		}
	}

	public function init() {

		if(!isset($this->domain)) {
			throw new Exception("Fields: No text domain has been set");
		}

		$note_data = [];
		$menu_order = 0;

		foreach($this->getSettings() as $setname => $data) {

			$menu_order++;

			$key = \Doctrine\Common\Inflector\Inflector::tableize(str_replace(' ', '', $setname));

			$_fields = [];
			$_location = [];
			$_hide_on_screen = [];

			if(!isset($data['location'])) {
				throw new \Exception("Fields: Missing location definition within {$name}");
			}

			if(!isset($data['fields'])) {
				throw new \Exception("Fields: Missing fields definition within {$name}");
			}

			foreach($data['location'] as $loc_set) {
				$ex = explode(",", $loc_set);
				$tmp = [];
				foreach($ex as $loc_rule) {
					$_ex = explode(" ", $loc_rule);
					$tmp[] = ['param' => $_ex[0], 'operator' => $_ex[1], 'value' => $_ex[2]];
				}
				$_location[] = $tmp;
			}

			if(isset($data['hide_on_screen'])) {
				$_hide_on_screen = $data['hide_on_screen'];
			}

			foreach($data['fields'] as $name => $fieldset) {

				if(isset($fieldset['i'])) {
					$fieldset['instructions'] = $fieldset['i'];
					unset($fieldset['i']); //shorthand for instructions
				}

				$info = array_merge($fieldset, [
					'key' => 'field_'.$key.'_'.$name,
					'label' => $this->humanize($name),
					'name' => $name
				]);

				$_fields[] = $info;
			}

			$register = [
				'key' => 'group_'.$key,
				'title' => $setname,
				'fields' => $_fields,
				'location' => $_location,
				'hide_on_screen' => $_hide_on_screen,
				'menu_order' => $menu_order
			];

			$note_data[] = $register;

			\register_field_group($register);
		}

		if(isset($_GET['fields_info'])) {
			$note = <<<NOTE
<h2>Custom Fields</h2>
<p>ACF Fields available within templates:</p>
<p><b>Guide:</b><br/>
Key: used internally within the code to refer to this value<br/>
Type: The type of input provided for this data, e.g. "text", "textarea (multiple lines)", "date", "relationship" (relates one item to another), "number" etc. </p>
<p>Each section also states "Available under the following conditions..." - this is a technical documentation that describes on what administrative pages a particular
set of fields will show up on. The "global fields", for example, show up on all pages and the event categories, location categories</p>
NOTE;
			foreach($note_data as $data) {
				$or_group = [];
				foreach($data['location'] as $and_group) {
					foreach($and_group as $group) {
						$or_group[] = "{$group['param']} {$group['operator']} {$group['value']}";
					}					
				}
				$for = "";
				foreach($or_group as $row) {
					$for.= "<code>{$row}</code>";
				}
				
				$note.= <<<NOTE
<h3>{$data['title']}</h3>
<p>These field variables will become available under the following conditions: {$for}</p>

<table width="100%">
	<thead>
		<tr>
			<th width="20%">Name</th>
			<th width="15%">Key</th>			
			<th width="15%">Type</th>
			<th width="50%">Instructions</th>
		</tr>
	</thead>
	<tbody>
NOTE;
				foreach($data['fields'] as $field) {
					$instruction = isset($field['instructions'])?$field['instructions']:'None';
					$note.= <<<NOTE
		<tr>			
			<td>{$field['label']}</td>
			<td>{$field['name']}</td>
			<td>{$field['type']}</td>
			<td>{$instruction}</td>
		</tr>
NOTE;
					
				}
				$note .= "</tbody></table>";

			}
			$this->set_note($note);
		}

	}


}
