<?php namespace Solarise\RapidConfig;

/**
 * Creating a custom Exception class to handle errors cleanly
 */
class Exception extends \Exception
{

	private $fatal;

    // Redefine the exception so message isn't optional
    public function __construct($message, $code = 0, Exception $previous = null) {
        parent::__construct($message, $code, $previous);
    }

    // custom string representation of object
    public function __toString() {
        return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
    }

    public function notFatal()
    {
    	$this->fatal = false;
    }

    public function isFatal()
    {
    	return $this->fatal;
    }

}