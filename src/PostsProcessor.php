<?php namespace Solarise\RapidConfig;

class PostsProcessor extends Processor implements IProcessor {

	public $name = 'posts';

	public function init() {

		if(!isset($this->domain)) {
			throw new Exception("Post type: No text domain has been set");
		}

		$notes = [];
		
		foreach($this->getSettings() as $key => $data) {

			if(!isset($data['labels']) || empty($data['labels'])) {
				$data['labels'] = [];
			}

			$singular = $this->singular($key);
			$plural = $this->plural($key);

			$_labels = array(
				'name'               => _x( ucfirst($plural), 'post type general name', $this->domain ),
				'singular_name'      => _x( ucfirst($singular), 'post type singular name', $this->domain ),
				'menu_name'          => _x( ucfirst($plural), 'admin menu', $this->domain ),
				'name_admin_bar'     => _x( ucfirst($singular), 'add new on admin bar', $this->domain ),
				'add_new'            => _x( 'Add New', $singular, $this->domain ),
				'add_new_item'       => __( 'Add New '.ucfirst($singular), $this->domain ),
				'new_item'           => __( 'New '.ucfirst($singular), $this->domain ),
				'edit_item'          => __( 'Edit '.ucfirst($singular), $this->domain ),
				'view_item'          => __( 'View '.ucfirst($singular), $this->domain ),
				'all_items'          => __( 'All '.ucfirst($plural), $this->domain ),
				'search_items'       => __( 'Search '.ucfirst($plural), $this->domain ),
				'parent_item_colon'  => __( 'Parent '.ucfirst($plural), $this->domain ),
				'not_found'          => __( 'No '.ucfirst($plural).' found.', $this->domain ),
				'not_found_in_trash' => __( 'No '.ucfirst($plural).' found in Recycling.', $this->domain )
			);

			$labels = array_merge($_labels, $data['labels']);

			if(!isset($data['prepend_slug']) || empty($data['prepend_slug'])) {
				$data['prepend_slug'] = '';
			}

			if(!isset($data['slug']) || empty($data['slug'])) {
				$slug = $this->plural_key($key);
			} else {
				$slug = $data['slug'];
			}

			

			if(!isset($data['args']) || empty($data['args'])) {
				$data['args'] = [];
			}

			$config_notes = "<ul>";
			foreach($data['args'] as $k => $arg) {
				if($arg === false) { $label = "No"; }
				elseif($arg === true) { $label = "Yes"; }
				else { $label = $arg; }
				$config_notes .= "<li>{$k}: {$label}</li>";
			}
			$config_notes .= "</ul>";

			if(isset($data['i'])) {
				$description = $data['i'];
			} else {
				$description = "None available";
			}

			//todo: ensure that it's possible not to have some post types
			//show up under single.php!!!!!
			$_args = array(
				'labels'             => $labels,
				'exclude_from_search'=> true,
				'public'             => true,
				'publicly_queryable' => true,
				'show_ui'            => true,
				'show_in_menu'       => true,
				'query_var'          => true,
				'rewrite'            => array( 'slug' => $data['prepend_slug'].$slug ),
				'capability_type'    => 'post',
				'has_archive'        => true,
				'has_single'		 => true, //not used by wordpress, used by RapidConfig to disable specific single-*.php files
				'hierarchical'       => false,
				'menu_position'      => null,
				'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' )
			);

			$args = array_merge($_args, $data['args']);

			$notes[] = <<<NOTE
		<tr>
			<td>{$singular}</td>
			<td>{$key}</td>
			<td>{$description}</td>
			<td>{$config_notes}</td>
		</tr>
NOTE;

			if($args['has_archive'] === true) {
				$this->add_theme_file("archive-{$key}.php");
			}

			if($args['has_single'] === true) {
				$this->add_theme_file("single-{$key}.php");
			}

			$this->add_theme_file("content-{$key}.php");

			\register_post_type($key, $args);
		}

		$full_notes = <<<NOTES
<h2>Custom Content Types</h2>
<p>Note: Unless otherwise specified, the following configuration options apply to all custom post types</p>
<p>
	<ul>
		<li>Data is publicly visible</li>
		<li>Visible in search results</li>
		<li>Available for administration to edit</li>
		<li>Standard URL naming rules apply</li>
		<li>Provides an archive page for listings (archive.php)</li>
		<li>Does <b>not</b> provide a hierarchial structure (pages = hierarchial, blog posts = non-hierarchial)</li>
		<li>Editor page will show the title, content editor, author, featured thumbnail, excerpt and comments functionality</li>
		<li>No specific priority given to menu positioning</li>
	</ul>
</p>
<p>The following custom data/post types are registered within the system:</p>
<table width="100%">
	<thead>
		<tr>
			<th width="15%">Post Name</th>
			<th width="15%">Post Slug</th>
			<th width="35%">Description</th>
			<th width="35%">Additional Config</th>
		</tr>
	</thead>
	<tbody>
NOTES;

		foreach($notes as $note) {
			$full_notes .= $note;
		}

		$full_notes .= "</tbody></table>";

		$this->set_note($full_notes);

	}

}