<?php namespace Solarise\RapidConfig;

use Doctrine\Common\Inflector;
use Symfony\Component\Yaml\Yaml;
 
class Processor {

	public $domain = false;

	//store any additional theme files that would be required after processing of this
	private $theme_files = [];

	private $settings = false;

	/**
	 * Returns the name of a processor instance. This is required within Main to 
	 * properly load configuration files.
	 * @return [type] [description]
	 */
	public function get_name()
	{
		return $this->name;
	}

	/**
	 * ?
	 * @param [type] $domain [description]
	 */
	public function set_domain($domain) {
		$this->domain = $domain;
	}

	/**
	 * Convert underscored names into human readable (capitalized) strings
	 * @param  string $string 	The string to be made readable
	 * @return string 			The humanized string
	 */
	public function humanize($string) {
		return ucwords(preg_replace('#[\-\_]+#is', ' ', $string));
	}

	/**
	 * Wrapper for the humanize function
	 * @param  string $string 	The string to be made readable
	 * @return string 			The humanized string
	 */
	public function singular($string) {
		return $this->humanize($string);
	}

	/**
	 * Converts any string into its plural version, with humanization
	 * @param  string $string 	The string to be pluralized
	 * @return string 			The pluralized string, with humanize()
	 */
	public function plural($string) {
		return \Doctrine\Common\Inflector\Inflector::pluralize($this->humanize($string));
	}

	/**
	 * Converts a string into its plural version, without humanization
	 * (used for data keys)
	 * @param  string $string 	The string to be pluralized
	 * @return string 			The pluralized string
	 */
	public function plural_key($string) {
		return \Doctrine\Common\Inflector\Inflector::pluralize($string);
	}

	/**
	 * Add a theme file into the processor
	 * @param string $file 		The theme file to add
	 */
	public function add_theme_file($file) {
		$this->theme_files[] = $file;
	}

	/**
	 * Return the theme files currently added into the processor
	 * @return array 			The array of theme files
	 */
	public function get_theme_files() {
		return $this->theme_files;
	}

	/**
	 * Sets a note for the processor, to be displayed within the documentation
	 * @param string $notes  	The notes to be added into the documentation
	 */
	public function set_note($notes) {
		$this->notes = $notes;
	}

	/**
	 * Retrieve the notes for this processor, used for documentation
	 * @return string
	 */
	public function get_notes() {
		return $this->notes;
	}

	/**
	 * Read a specified YAML file
	 */
	public function setYAML($file)
	{
		if(is_readable($file)) {
			$this->settings = Yaml::parse($file);
		} else {
			$this->settings = false;
		}
	}

	/**
	 * Retrieve the settings data
	 */
	public function getSettings()
	{
		if(!$this->settings) {
			$e = new Exception("No configuration data specified");
			$e->notFatal();
			throw $e;
		}
		return $this->settings;
	}

}