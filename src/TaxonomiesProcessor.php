<?php namespace Solarise\RapidConfig;

class TaxonomiesProcessor extends Processor implements IProcessor {

	public $name = 'taxonomies';

	function init() {

		if(!isset($this->domain)) {
			throw new \Exception("Taxonomy: No text domain has been set");
		}

		// Add new taxonomy, make it hierarchical (like categories)
		foreach($this->getSettings() as $key => $data) {

			if(!isset($data['labels']) || empty($data['labels'])) {
				$data['labels'] = [];
			}

			//todo: DRY
			$singular = $this->singular($key);
			$plural = $this->plural($key);

			$_labels = array(
				'name'              => _x( ucwords($plural), 'taxonomy general name' ),
				'singular_name'     => _x( ucwords($singular), 'taxonomy singular name' ),
				'search_items'      => __( 'Search '.ucwords($plural) ),
				'all_items'         => __( 'All '.ucwords($plural) ),
				'parent_item'       => __( 'Parent '.ucwords($singular) ),
				'parent_item_colon' => __( 'Parent '.ucwords($singular) ),
				'edit_item'         => __( 'Edit '.ucwords($singular) ),
				'update_item'       => __( 'Update '.ucwords($singular) ),
				'add_new_item'      => __( 'Add New '.ucwords($singular) ),
				'new_item_name'     => __( 'New '.ucwords($singular).' Name' ),
				'menu_name'         => __( ucwords($plural) ),
			);

			$labels = array_merge($_labels, $data['labels']);

			if(!isset($data['args']) || empty($data['args'])) {
				$data['args'] = [];
			}

			$config_notes = "<ul>";
			foreach($data['args'] as $k => $arg) {
				
				$config_notes .= "<li>{$k}: {$arg}</li>";
			}
			$config_notes .= "</ul>";

			$_args = array(
				'hierarchical'      => true,
				'labels'            => $labels,
				'show_ui'           => true,
				'show_admin_column' => true,
				'has_archive'		=> false, //not used by WP, used internally by RapidConfig
				'query_var'         => true,
				'rewrite'           => array( 'slug' => $this->plural_key($key) ),
			);

			if(!isset($data['post-types'])) {
				$data['post-types'] = ['post'];
			}

			$args = array_merge($_args, $data['args']);

			$post_types = implode(", ", $data['post-types']);

			if(isset($data['i'])) {
				$description = $data['i'];
			} else {
				$description = "None available";
			}

			$notes[] = <<<NOTE
		<tr>
			<td>{$singular}</td>
			<td>{$key}</td>
			<td>{$post_types}</td>
			<td>{$description}</td>
			<td>{$config_notes}</td>
		</tr>
NOTE;
			if($args['has_archive'] === true) {
				$this->add_theme_file("taxonomy-{$key}.php");
				if(isset($data['custom-template'])) {
					foreach($data['custom-template'] as $template) {
						//set unique layouts for specific terms
						$this->add_theme_file("taxonomy-{$key}-{$template}.php");
					}
				}
			}

			\register_taxonomy($key, $data['post-types'], $args );
		}

		$full_notes = <<<NOTES
<h2>Custom Categorisation</h2>
<p>Note: Unless otherwise specified, the following configuration options apply to all categories</p>
<p><b>Key:</b><br/>
Category Slug: Used within the code (not relevant for administration)<br/>
Affected Content: Lists the post types to which this category applies<br/>
Additional Config: Extra parameters, applies mostly within the code but also can be used by admin to determine how a particular category will function (e.g. "has_archive:1" indicates that a category has a main landing page on which multiple sub-categories or items are to be available)
<p>
	<ul>
		<li>Hierarchial (i.e. can have sub-categories)</li>
		<li>Visible to administration</li>
		<li>Queryable via front-end</li>
		<li>Standard URL naming rules apply</li>
	</ul>
</p>
<p>The following custom categorisation settings are configured:</p>
<table width="100%">
	<thead>
		<tr>
			<th width="15%">Category Name</th>
			<th width="15%">Category Slug</th>
			<th width="20%">Affected content</th>
			<th width="25%">Description</th>
			<th width="25%">Additional Config</th>
		</tr>
	</thead>
	<tbody>
NOTES;

		foreach($notes as $note) {
			$full_notes .= $note;
		}

		$full_notes .= "</tbody></table>";

		$this->set_note($full_notes);

	}
}


// - - - - - - - - - - 