<?php namespace Solarise\RapidConfig;


if ( !defined('ABSPATH') ) {
		die('RapidConfig requires that the WP environment is loaded');
}

/**
 * Main class definition
 */
class Main {

	private $processors = [];
	private $settings = [];
	private $errors = [];
	private $domain = 'default-domain';
	private $notes = [];

	//List of basic files to auto-generate for the theme
	//Can be overriden on the theme level
	private $theme_files = [
		'404.php',
		'archive.php',
		'comments.php',
		'content-none.php',
		'content.php',
		'footer.php',
		'functions.php',
		'page.php',
		'search.php',
		'searchform.php',
		'sidebar.php',
		'single.php'
	];

	function __construct() {
		add_action('admin_notices', [$this, 'admin_notice']);
	}

	/**
	 * Sets the text domain for all processors
	 * @param string $domain  	The string to set as the text domain
	 */
	function set_domain($domain)
	{
		$this->domain = $domain;
	}

	/**
	 * Display error notices within admin
	 */
	public function admin_notice() {
	    ?>
	    <div class="error">
	    	<?php
	    	foreach($this->errors as $error) {
	    		echo '<p>'._e($error, $this->domain).'</p>';
	    	}
	    	?>	       
	    </div>
	    <?php
	}

	/**
	 * Output the notes as defined within each of the processors.
	 * Used for auto-generation of documentation
	 */
	public function notes() {

		if(!current_user_can('manage_options')) {
			return;
		}

		if(isset($_GET['rapid_config_doc'])) {
			$html = <<<HTML
<!DOCTYPE html>
<html>
<head>
	<meta charset=utf-8 />
	<title></title>
	<link rel="stylesheet" type="text/css" media="screen" href="//cdnjs.cloudflare.com/ajax/libs/foundation/5.4.5/css/foundation.css" />
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>
	<!--[if IE]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
</head>
<body>
<div class="row"><div class="columns large-12">
<h2>Custom Functionality Specification</h2>
<p>This document briefly outlines the additional administrative functionality that has been added to the website, including:</p>
<p>
<ul>
<li>A list of all the custom content types</li>
<li>All the custom categorisations</li>
<li>Additional data-entry fields attached to each custom content type</li>
<li>The image sizes available for use within the code of the site (for development use only)</li>
<li>An outline of the template file structure (for development use only)</li>
</ul>
</p>
<p>This document may be used as either a technical guide or as an overview of the administrative functionality, describing input fields and relationships between datatypes</p>
HTML;

			foreach($this->notes as $note) {
				$html .= $note;
			}

			$html .= <<<FILES
<h2>Template File Configuration</h2>
<p>The following table shows the suggested file structure for the template</p>
<table width="100%">
	<thead>
		<tr>
			<th width="40%">Template filename</th>
			<th width="15%">File exists</th>
		</tr>
	</thead>
	<tbody>
FILES;

			foreach($this->theme_files as $file) {
				$file_name = get_template_directory().'/'.$file;
				if(isset($_GET['gen_files'])) {
					//write data to this file
					if(strpos($file, 'single') === 0) {
						$template = 'single.php';
					} elseif(strpos($file, 'archive') === 0) {
						$template = 'archive.php';
					} elseif(strpos($file, 'taxonomy') === 0) {
						$template = 'taxonomy.php';
					} elseif(strpos($file, 'taxonomy') === 0) {
						$template = 'content.php';
					} elseif($file == '404.php') {
						$template = '404.php';
					} elseif($file == 'header.php') {
						$template = 'header.php';
					} elseif($file == 'footer.php') {
						$template = 'footer.php';
					} elseif($file == 'sidebar.php') {
						$template = 'sidebar.php';
					} elseif($file == 'search.php') {
						$template = 'search.php';
					} elseif($file == 'page.php') {
						$template = 'page.php';
					}
				}
				if(is_readable($file_name)) {
					$exists = '<span class="success">File exists</span>';
				} else {
					//create file
					$fp = fopen($file_name, 'w');
					fwrite($fp, '<!-- nothing here -->');
					fclose($fp);
					$exists = '<span class="warning">File not found</span>';
				}
				$code_status = '<code>$var_name = false;</code>';
				$html .= <<<ROW
		<tr>
			<td>{$file}</td>
			<td>{$exists}</td>
		</tr>
ROW;
			}

			$html .=  "</tbody></table>";

			$html .=  "</div></div></body></html>";
			if(isset($_GET['pdf'])) {
				$html2pdf = new \HTML2PDF('P', 'A4');
	        	$html2pdf->writeHTML($html);
	        	$html2pdf->output('documentation.pdf');
			} else {
				echo $html;
			}			
			exit;
		}
	}

	/**
	 * Add a processor
	 * @param \Solarise\RapidConfig\IProcessor $processor  The processor to add into the process queue and to generate documentation from
	 */
	public function add_processor(\Solarise\RapidConfig\IProcessor $processor) {

		try {

			$processor->set_domain($this->domain);
			foreach($processor->get_theme_files() as $file) {
				$this->theme_files[] = $file;
			}
			$this->processors[] = $processor;

		} catch(Exception $e) {
			$this->errors[] = $e;
		}

	}

	public function apply_processors()
	{
		foreach($this->processors as $processor) {
			try {
				$processor->init();
				$this->notes[] = $processor->get_notes();
			} catch(Exception $e) {
				if($e->isFatal())
				{
					throw $e;
				}
			}
		}
		// check for notes request
		$this->notes();
	}

	/**
	 * Apply all the registered processors upon WP init
	 */
	public function init()
	{
		add_action('init', array($this, 'apply_processors'));
	}

}