<?php namespace Solarise\RapidConfig;

class SizesProcessor extends Processor implements IProcessor {

	public $name = 'sizes';

	public function init() {

		if(!isset($this->domain)) {
			throw new Exception("Image Size: No text domain has been set");
		}

		$notes = [];

		foreach($this->getSettings() as $key => $data) {

			if(!isset($data['w'])) {
				throw new Exception('Width not specified for '.$key);
			}

			if(!isset($data['h'])) {
				throw new Exception('Height not specified for '.$key);
			}

			if(!isset($data['fit'])) {
				$data['fit'] = true;
			}

			$fit = $data['fit'] ? 'Scale to fit' : 'Crop to fit';

			$notes[] = <<<NOTE
		<tr>
			<td>{$key}</td>
			<td>{$data['w']}x{$data['h']}</td>
			<td>{$fit}</td>
			<td><code>the_post_thumbnail('{$key}')</code></td>
		</tr>
NOTE;

			\add_image_size($key, $data['w'], $data['h'], $data['fit']);
			
		}

		$full_notes = <<<NOTES
<h2>Image Sizes</h2>
<p>The following image sizes are configured within the system</p>
<table width="100%">
	<thead>
		<tr>
			<th width="25%">Ref</th>
			<th width="15%">Size</th>
			<th width="15%">Scale</th>
			<th width="45%">Code</th>
		</tr>
	</thead>
	<tbody>
NOTES;

		foreach($notes as $note) {
			$full_notes .= $note;
		}

		$full_notes .= "</tbody></table>";

		$this->set_note($full_notes);
	}

}