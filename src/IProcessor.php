<?php namespace Solarise\RapidConfig;

interface IProcessor {

	public function init();

	public function setYAML($file);

	public function get_name();
	
	public function set_domain($domain);

}
