<?php
/**
 * @package solarise/rapid-config
 */
/*
Plugin Name: Rapid Config for Wordpress
Plugin URI: http://solarisedesign.co.uk
Description: Rapid Config is a configuration tool for Wordpress to quickly define post types, categories and more within a simple YAML formatted file. For examples, see solarisedesign.co.uk/articles/rapid-config-for-wordpress
Version: 1.0.0
Author: Robin Metcalfe
Author URI: http://solarisedesign.co.uk/about
License: MIT
Text Domain: rapidconfig
*/

require_once('vendor/autoload.php');

$RapidConfig = new Solarise\RapidConfig\Main;
$RapidConfig->set_domain('rapidconfig');

$posts = new Solarise\RapidConfig\PostsProcessor;
$posts->setYAML(WP_CONTENT_DIR . '/rapid-config/posts.yaml');
$RapidConfig->add_processor($posts);

$taxonomies = new Solarise\RapidConfig\TaxonomiesProcessor;
$taxonomies->setYAML(WP_CONTENT_DIR . '/rapid-config/taxonomies.yaml');
$RapidConfig->add_processor($taxonomies);

$sizes = new Solarise\RapidConfig\SizesProcessor;
$sizes->setYAML(WP_CONTENT_DIR . '/rapid-config/sizes.yaml');
$RapidConfig->add_processor($sizes);

$fields = new Solarise\RapidConfig\FieldsProcessor;
$fields->setYAML(WP_CONTENT_DIR . '/rapid-config/fields.yaml');
$RapidConfig->add_processor($fields);

// Initialise all
$RapidConfig->init();