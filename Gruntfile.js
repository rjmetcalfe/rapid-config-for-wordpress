/*global module:false*/
module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({

    pkg: grunt.file.readJSON('package.json'),
    env: grunt.file.readJSON('env.json'),
    

    clean: {
      main: ['tmp/<%= pkg.version %>'],
      composer: ['tmp/<%= pkg.version %>/composer.*']
    },

    copy: {
      // Copy the plugin to a versioned release directory
      main: {
        files: [{
          expand: true,
          src:  [
            '**',
            '!node_modules/**',
            '!release/**',
            '!tmp/**',
            '!.git/**',
            '!composer.lock',
            '!vendor/**',
            '!env.*',
            '!npm-debug.log',
            '!.sass-cache/**',
            '!css/src/**',
            '!js/src/**',
            '!img/src/**',
            '!Gruntfile.js',
            '!package.json',
            '!.gitignore',
            '!.gitmodules'
          ],
          // place it in tmp initially - this will all be combined
          // with composer /vendor --no-dev and zipped into /release
          dest: 'tmp/<%= pkg.version %>/'
        }]
      }   
    },

    composer : {      
      development: {
        options : {
          usePhp: true,
          flags: ['no-dev'],
          cwd: './tmp/<%= pkg.version %>/',
          composerLocation: '<%= env.composer_location %>'
        },
      }
    },

    compress: {
      main: {
        options: {
          mode: 'zip',
          // store all of the previous released zip files
          archive: './release/<%= pkg.name %>.<%= pkg.version %>.zip'
        },
        expand: true,
        cwd: 'tmp/<%= pkg.version %>/',
        src: ['**/*'],
        dest: '<%= pkg.name %>/'
      }   
    },

    phpunit: {
      classes: {
        dir: 'tests'
      },
      options: {
        bin: 'vendor/bin/phpunit',
        bootstrap: 'tests/bootstrap.php',
        colors: true
      }
    }

 
  });

  grunt.loadNpmTasks('grunt-phpunit');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-composer');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-compress');

  grunt.registerTask( 'test', ['phpunit'] );
 
  grunt.registerTask( 'build', ['phpunit', 'clean', 'copy', 'composer:development:install', 'clean:composer', 'compress', 'clean'] );

};
